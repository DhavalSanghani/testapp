import React, {useState, useEffect, useRef} from 'react';
import {
  View,
  Dimensions,
  Text,
  ScrollView,
} from 'react-native';

import {useSelector, useDispatch, shallowEqual} from 'react-redux';

import {
  getFilmsDataAction,
  resetAction,
} from '../actions/SideMenuAction';


const windowSize = Dimensions.get('window');

const FilmDetails = ({navigation, route}) => {
  const dispatch = useDispatch();


  const data = useSelector((state) => state, shallowEqual);
  const {filmsData, responseCode} = data.sideMenuReducers;

  const [listDataSource, setListDataSource] = useState([]);
  const [isloading, setisLoading] = useState(false);

  useEffect(() => {

    if (filmsData == null) {
      setisLoading(true);
      dispatch(getFilmsDataAction(route.params.films));
    }
    if (filmsData != null) {
      setisLoading(false);
      setListDataSource(filmsData);

    }
  }, [filmsData]);

  

  const onRightButtton = () => {
    alert('Right');
  };

  return (
    <View style={{flex: 1, backgroundColor: 'white'}}>
      {isloading && <Progressbar />}

      <Header
        name={'Film Details'}
        navigation={navigation}
        rightButton={onRightButtton}
      />
      {!isloading ? (
        <ScrollView>
          <View
            style={{
              flex: 1,
              // justifyContent: 'space-between',
              padding: 8,
            }}>
            <Text style={{color: 'black', fontWeight: 'bold', fontSize: 16}}>
              {'Producer : '}
            </Text>
            <Text style={{fontSize: 22}}>{listDataSource.producer}</Text>
            <Text style={{color: 'black', fontWeight: 'bold', fontSize: 16}}>
              {'Created : '}
            </Text>
            <Text style={{fontSize: 18}}>{listDataSource.created}</Text>
            <Text style={{color: 'black', fontWeight: 'bold', fontSize: 16}}>
              {'Release Date : '}
            </Text>
            <Text style={{fontSize: 18}}>{listDataSource.release_date}</Text>
            <Text style={{color: 'black', fontWeight: 'bold', fontSize: 16}}>
              {'Director  : '}
            </Text>
            <Text style={{fontSize: 18}}>{listDataSource.director}</Text>
            <Text style={{color: 'black', fontWeight: 'bold', fontSize: 16}}>
              {'Edited  : '}
            </Text>
            <Text style={{fontSize: 18}}>{listDataSource.edited}</Text>
            <Text style={{color: 'black', fontWeight: 'bold', fontSize: 16}}>
              {'Episode Id : '}
            </Text>
            <Text style={{fontSize: 18}}>{listDataSource.episode_id}</Text>
            <Text style={{color: 'black', fontWeight: 'bold', fontSize: 16}}>
              {'Opening Crawl : '}
            </Text>
            <Text style={{fontSize: 18}}>{listDataSource.opening_crawl}</Text>
            {listDataSource.characters ? (
              <View
                style={{
                  // height: layoutHeight,
                  overflow: 'hidden',
                  marginTop: 20,
                }}>
                <Text
                  style={{color: 'black', fontWeight: 'bold', fontSize: 16}}>
                  {'Characters Details : '}
                </Text>

                {listDataSource.characters.map((item, index) => (
                  <Text key={index} style={[styles.text, {color: '#000000'}]}>
                    {item}
                  </Text>
                ))}
              </View>
            ) : null}
            {listDataSource.planets ? (
              <View
                style={{
                  // height: layoutHeight,
                  overflow: 'hidden',
                  marginTop: 20,
                }}>
                <Text
                  style={{color: 'black', fontWeight: 'bold', fontSize: 16}}>
                  {'Planets Details : '}
                </Text>

                {listDataSource.planets.map((item, index) => (
                  <Text key={index} style={[styles.text, {color:'#000000'}]}>
                    {item}
                  </Text>
                ))}
              </View>
            ) : null}
          </View>
        </ScrollView>
      ) : null}
    </View>
  );
};

const styles = {
  listContainer: {
    flex: 1,
    minHeight: (windowSize.height * 20) / 100,

    margin: 8,
    // padding: 8,
    borderWidth: 1,
    shadowColor: 'grey',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
  },
};

export default FilmDetails;
