import React from 'react';
import {useState, useEffect} from 'react';
import {
  View,
  Image,
  Dimensions,
  Text,
  FlatList,
  TouchableOpacity,
} from 'react-native';
import Header from '../components/Header';

import {getSideMenuDataAction} from '../actions/SideMenuAction';
import {API_SIDE_MENU} from '../utils/constants/URLs';

import {useSelector, useDispatch, shallowEqual} from 'react-redux';


const windowSize = Dimensions.get('window');

const Home = ({props, navigation}) => {

  const dispatch = useDispatch();
  const data = useSelector((state) => state, shallowEqual);

  const {menuData, responseCode} = data.sideMenuReducers;
  const [listDataSource, setListDataSource] = useState([]);

  useEffect(() => {
    if (menuData == null) {
      dispatch(getSideMenuDataAction(API_SIDE_MENU));
    }
    if (menuData != null) {
      setListDataSource(menuData);
      // dispatch(resetAction());
    }
  }, [menuData]);

  const onPeopleDetails = (item) => {
    navigation.navigate('PeopleDetails', {
      peopleDetail: item,
    });
  };

  const onRightButtton = () => {
    alert('Right');
  };

  const renderItem = ({item, index}) => {
    return (
      <TouchableOpacity
        style={[
          styles.listContainer,
          {
            backgroundColor:'white',
            borderColor: '#ED7323',
            shadowColor: 'grey',
          },
        ]}
        activeOpacity={1}
        onPress={() => onPeopleDetails(item)}>
        <View
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
            flexDirection: 'row',
          }}>
          <Text style={{color: 'black', fontSize: 16}}>{item.name}</Text>
        </View>
      </TouchableOpacity>
    );
  };

  return (
    <View style={{flex: 1, backgroundColor: 'white'}}>
      <Header navigation={navigation} rightButton={onRightButtton} />

      <FlatList
        numColumns={2}
        style={{paddingVertical: 5}}
        data={listDataSource}
        keyExtractor={(item, index) => index.toString()}
        renderItem={renderItem}
      />
    </View>
  );
};
const styles = {
  listContainer: {
    flex: 1,
    minHeight: (windowSize.height * 20) / 100,
    margin: 8,
    padding: 8,
    borderWidth: 1,
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
};

export default Home;
