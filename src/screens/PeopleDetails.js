import React from 'react';
import {useState, useEffect} from 'react';
import {
  View,
  Image,
  Dimensions,
  Text,
  FlatList,
  TouchableOpacity,
} from 'react-native';
import Header from '../components/Header';

import Progressbar from '../components/Progressbar';
import {useSelector, useDispatch, shallowEqual} from 'react-redux';

const windowSize = Dimensions.get('window');

const PeopleDetails = ({navigation, route}) => {
  const dispatch = useDispatch();
  const data = useSelector((state) => state, shallowEqual);

  const [isloading, setisLoading] = useState(false);

  const onRightButtton = () => {
    alert('Right');
  };

  const onFilms = (item) => {
    navigation.navigate('FilmDetails', {
      films: item,
    });
  };

  return (
    <View style={{flex: 1, backgroundColor: 'white'}}>
      {isloading && <Progressbar />}

      <Header
        name={'People Details'}
        navigation={navigation}
        rightButton={onRightButtton}
      />

      <View
        style={{
          flex: 1,
          // justifyContent: 'center',
          // alignItems: 'center',
          // flexDirection: 'row',
          margin: 20,
        }}>
        <Text style={{color: '#000000', fontSize: 16}}>
          {'Name : ' + route.params.peopleDetail.name}
        </Text>

        <Text style={{color: 'black', fontSize: 16}}>
          {'Birth Year : ' + route.params.peopleDetail.birth_year}
        </Text>
        <Text style={{color: 'black', fontSize: 16}}>
          {'Created : ' + route.params.peopleDetail.created}
        </Text>
        <Text style={{color: 'black', fontSize: 16}}>
          {'Edited : ' + route.params.peopleDetail.edited}
        </Text>
        <Text style={{color: 'black', fontSize: 16}}>
          {'Eye Color : ' + route.params.peopleDetail.eye_color}
        </Text>
        <Text style={{color: 'black', fontSize: 16}}>
          {'Gender : ' + route.params.peopleDetail.gender}
        </Text>
        <Text style={{color: 'black', fontSize: 16}}>
          {'Hair Color : ' + route.params.peopleDetail.hair_color}
        </Text>
        <Text style={{color: 'black', fontSize: 16}}>
          {'Height : ' + route.params.peopleDetail.height}
        </Text>
        <Text style={{color: 'black', fontSize: 16}}>
          {'Mass : ' + route.params.peopleDetail.mass}
        </Text>
        <Text style={{color: 'black', fontSize: 16}}>
          {'Skin Color : ' + route.params.peopleDetail.skin_color}
        </Text>

        <View
          style={{
            overflow: 'hidden',
            marginTop: 20,
          }}>
          <Text style={{color: 'black', fontSize: 16}}>
            {'Films Details : '}
          </Text>

          {route.params.peopleDetail.films.map((item, key) => (
            <TouchableOpacity
              key={key}
              style={[
                styles.content,
                {
                  backgroundColor: 'white',
                  color: '#000000',
                  borderRadius: 20,
                  borderWidth: 0.2,
                  borderColor: 'blue',
                  margin: 10,
                },
              ]}
              onPress={() => onFilms(item)}>
              <Text style={[styles.text, {color:'#000000'}]}>
                {item}
              </Text>
              <View style={[styles.separator, {colors: 'red'}]} />
            </TouchableOpacity>
          ))}
        </View>
      </View>

     
    </View>
  );
};
const styles = {
  listContainer: {
    flex: 1,
    minHeight: (windowSize.height * 20) / 100,
    // flexDirection: 'row',
    margin: 8,
    padding: 8,
    borderWidth: 1,
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
  },
  text: {
    fontSize: 16,
    paddingLeft: '10%',
    paddingVertical: 5,
  },
};

export default PeopleDetails;
