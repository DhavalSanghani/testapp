import {Dimensions} from 'react-native';
const {width, height} = Dimensions.get('window');

const guidelineBaseWidth = 350;
const guidelineBaseHeight = 680;

const hScale = (size) => (width / guidelineBaseWidth) * size;
const vScale = (size) => (height / guidelineBaseHeight) * size;
const mScale = (size, factor = 0.5) => size + (hScale(size) - size) * factor;

export {hScale, vScale, mScale}; //horizontal vertical moderate
const fontSize = mScale(17);

export const lightTheme = {
  primaryText: '#ffffff',
  background: '#000000',
  primaryColor: '#ED7323',
  hint: 'rgba(153, 153, 153, 1)',

  subCategoryColor: '#232323',
  success: '#22c93d',
  error: '#e63729E5',
};

export const darkTheme = {
  primaryText: '#000000',
  background: '#ffffff',
  primaryColor: '#ED7323',
  hint: 'rgba(153, 153, 153, 1)',
  subCategoryColor: '#ffffff',

  success: '#22c93d',
  error: '#e63729E5',
};

export default Theme = {
  colors: {
    primaryText: '#262626',
    inversePrimaryText: '#000000',
    secondaryText: 'rgba(153, 153, 153, 1)',
    inverseSecondaryText: '#54504E',
    cardBackground: '#111111',
    borderTextInput: '#DCDDDD',
    // btnbackgrdColor: 'blue',
    borderColor: 'rgba(243, 243, 243, 1)',
    white: 'white',
    black: 'black',
    grey: 'grey',

    primary: '#424352',
    secondary: '#6e7172',
    btnBackgroundColor: 'orange',
    textColor: 'rgba(18, 45, 154, 1)',
    lightgrey: '#F2F2F2',
    lightblue: '#DCE5F2',
    transparent: 'transparent',

    highlight: '#4E5671',

    success: '#22c93d',
    error: '#e63729E5',
  },

  fontSizes: {
    minTiny: fontSize * 0.55, //9
    tiny: fontSize * 0.65, //11
    small: fontSize * 0.8, //14
    medium: fontSize * 1, //17
    large: fontSize * 1.1, //19
    xlarge: fontSize * 1.3, //22
    xxlarge: fontSize * 2.0, //34
    xxxlarge: fontSize * 3.2, //55
    srklogo: fontSize * 7.05, //120
    faceImage: fontSize * 15.88, //270
  },
  fontFamily: {
    srkIcons: 'SRKOne',
    rb_regular: 'Rubik-Regular',
    rb_medium: 'Rubik-Medium',
    rb_light: 'Rubik-Light',
  },
};
