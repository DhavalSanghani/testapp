import React from 'react';
import {View, Icon, Text, Image, TouchableOpacity} from 'react-native';

import {useSelector, shallowEqual} from 'react-redux';

import {DrawerActions} from '@react-navigation/native';

Header = (props, navigation) => {
  const data = useSelector((state) => state, shallowEqual);

  return (
    <View style={[styles.mainContainer, {backgroundColor: 'white'}]}>
      <TouchableOpacity
        onPress={() => props.navigation.dispatch(DrawerActions.toggleDrawer())}>
        <Image
          source={require('../utils/assets/Images/menu.png')}
          style={{width: 25, height: 25, marginLeft: 15}}
        />
      </TouchableOpacity>
      {props.name ? (
        <Text style={{fontWeight: 'bold', fontSize: 22}}>{props.name}</Text>
      ) : (
        <Image
          source={require('../utils/assets/Images/swap.png')}
          style={{width: 120, height: 40}}
        />
      )}
      <TouchableOpacity onPress={props.rightButton}>
        <Image
          source={require('../utils/assets/Images/menu.png')}
          style={{width: 25, height: 25, marginRight: 15}}
        />
      </TouchableOpacity>
    </View>
  );
};

const styles = {
  mainContainer: {
    // backgroundColor: '#ffffff',
    flexDirection: 'row',
    height: 50,
    fleX: 1,
    alignItems: 'center',
    justifyContent: 'space-between',
    shadowColor: 'red',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowOpacity: 0.37,
    shadowRadius: 7.49,

    elevation: 12,
  },
};
export default Header;
