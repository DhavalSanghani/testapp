import React from 'react';
import {Text, TouchableOpacity, StyleSheet} from 'react-native';
import {useSelector, shallowEqual} from 'react-redux';

Progressbar = (props) => {
  const data = useSelector((state) => state, shallowEqual);


  return (
    <TouchableOpacity
      activeOpacity={1}
      onPress={() => null}
      style={[styles.modal, {backgroundColor: 'rgba(153, 153, 153, 1)'}]}>
     
      <Text style={{fontSize:25,color:'white'}}>Loading....</Text>
    </TouchableOpacity>
  );
};

Progressbar.defaultProps = {
  isSmall: false,
};

const styles = StyleSheet.create({
  modal: {
    position: 'absolute',
    bottom: 0,
    height: '100%',
    width: '100%',
    flex: 1,
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    opacity: 0.8,
    zIndex: 1,
  },
  loaderImage: {
    alignSelf: 'center',
  },
});
export default Progressbar;
