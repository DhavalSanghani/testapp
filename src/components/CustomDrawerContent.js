// import React from 'react';
// import {Button} from 'react-native';

import React, {useState, useEffect} from 'react';
import {
  SafeAreaView,
  LayoutAnimation,
  StyleSheet,
  View,
  Text,
  ScrollView,
  UIManager,
  TouchableOpacity,
  Platform,
  Image,
} from 'react-native';

import {useSelector, useDispatch, shallowEqual} from 'react-redux';
import {getSideMenuDataAction} from '../actions/SideMenuAction';
import {API_SIDE_MENU} from '../utils/constants/URLs';

const CustomDrawerContent = ({navigation}) => {
  const [constructorHasRun, setConstructorHasRun] = useState(false);
  const dispatch = useDispatch();

  const data = useSelector((state) => state, shallowEqual);
  const {menuData, responseCode} = data.sideMenuReducers;


  const [listDataSource, setListDataSource] = useState([]);

  const [multiSelect, setMultiSelect] = useState(false);

  if (Platform.OS === 'android') {
    UIManager.setLayoutAnimationEnabledExperimental(true);
  }

  useEffect(() => {
    if (menuData == null) {

      dispatch(getSideMenuDataAction(API_SIDE_MENU));
    }
    if (menuData != null) {
      setListDataSource(menuData);
    }
  }, [menuData]);

  const updateLayout = (index) => {
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
    const array = [...listDataSource];
    array[index]['isExpanded'] = !array[index]['isExpanded'];
  
    setListDataSource(array);
  };

  const ExpandableComponent = ({item, onClickFunction}) => {
    const [layoutHeight, setLayoutHeight] = useState(0);

    useEffect(() => {
      if (item.isExpanded) {
        setLayoutHeight(null);
      } else {
        setLayoutHeight(0);
      }
    }, [item.isExpanded]);

    return (
      <View>
        <TouchableOpacity
          activeOpacity={0.8}
          onPress={onClickFunction}
          style={[
            styles.header,
            {
              minWidth: 40,
              flexDirection: 'row',
              backgroundColor: 'white',
              borderBottomColor:'rgba(153, 153, 153, 1)' ,
              borderWidth: 0.3,
              justifyContent: 'space-between',
              alignItems: 'center',
              marginHorizontal: 10,
            },
          ]}>
        
          <Text
            style={{
              fontSize: 20,
              flex: 0.92,
              marginLeft: '10%',
              // color:'white'
              color: item.isExpanded ? '#ED7323': 'rgba(153, 153, 153, 1)',
            }}>
            {item.name}
          </Text>

          {item.films.length > 0 ? (
            <Text
              style={{
                color: item.isExpanded ? '#ED7323' : 'rgba(153, 153, 153, 1)',
                fontSize: 28,
              }}>
              {item.isExpanded ? '-' : '+'}
            </Text>
          ) : (
            <Text style={{color: 'rgba(153, 153, 153, 1)', fontSize: 28}}></Text>
          )}
        </TouchableOpacity>

        <View
          style={{
            height: layoutHeight,
            overflow: 'hidden',
          }}>
          {item.films.map((item, key) => (
            <TouchableOpacity
              key={key}
              style={[
                styles.content,
                {
                  backgroundColor: 'white',
                  color: '#000000',
                },
              ]}
              onPress={() => {
                navigation.navigate('FilmDetails', {
                  films: item,
                });
              }}>
              <Text style={[styles.text, {color:'#000000'}]}>
                {item}
              </Text>
              <View style={[styles.separator, {colors: 'red'}]} />
            </TouchableOpacity>
          ))}
        </View>
      </View>
    );
  };
  return (
    <SafeAreaView style={{flex: 1}}>
      <View
        style={{
          minHeight: '20%',
          backgroundColor: '#ED7323',
          flexDirection: 'row',
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <Image
          source={require('../utils/assets/Images/swap.png')}
          style={{width: '100%', height: '100%'}}
        />
      </View>

      <View style={[styles.container, {backgroundColor: 'white'}]}>
        <ScrollView>
          {listDataSource.map((item, key) => (
            <ExpandableComponent
              key={item.name}
              onClickFunction={() => {
                updateLayout(key);
              }}
              item={item}
            />
          ))}
        </ScrollView>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
 
  header: {
    padding: 8,
  },
 
  separator: {
    height: 0.5,
    width: '95%',
    marginLeft: 16,
    marginRight: 16,
  },
  text: {
    fontSize: 16,
    paddingLeft: '10%',
    paddingVertical: 5,
  },
  content: {
    padding: 7,
    paddingLeft: '20%',
  },
});

export default CustomDrawerContent;
