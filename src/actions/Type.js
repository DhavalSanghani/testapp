export const LOGIN_SUCCESS = 'LOGIN_SUCCESS';
export const LOGIN_ERROR = 'LOGIN_ERROR';
export const LOGIN_FAIL = 'LOGIN_FAIL';

export const LIGHT_THEME = 'light';
export const DARK_THEME = 'dark';

export const ACTION_FAIL = 'action_fail';
export const RESET = 'RESET';

export const START_LOADING = 'start_loading';
export const STOP_LOADER = 'STOP_LOADER';

export const MENU_DATA_SUCCESS = 'MENU_DATA_SUCCESS';
export const MENU_DATA_ERROR = 'MENU_DATA_ERROR';
export const SUB_CATEGORY_DATA_SUCCESS = 'SUB_CATEGORY_DATA_SUCCESS';
export const SUB_CATEGORY_DATA_ERROR = 'SUB_CATEGORY_DATA_ERROR';

export const GET_PRODUCT_DATA_SUCCESS = 'GET_PRODUCT_DATA_SUCCESS';
export const GET_PRODUCT_DATA_ERROR = 'GET_PRODUCT_DATA_ERROR';
export const  GET_FILMDATA_SUCCESS = "GET_FILMDATA_SUCCESS";
export const GET_FILMDATA_ERROR = "GET_FILMDATA_ERROR";