import {
  RESET,
  MENU_DATA_SUCCESS,
  MENU_DATA_ERROR,
  GET_FILMDATA_SUCCESS,
  GET_FILMDATA_ERROR
} from '../actions/Type';

import {getRequestApi} from '../utils/service/ServiceManages';

export const resetAction = (type = 'NORMAL') => {
  return (dispatch) => dispatch({type: RESET, payload: type});
};

export function getSideMenuDataAction(url, test = false) {
  return (dispatch) =>
    getRequestApi({
      url,
      dispatch,
      actionType: MENU_DATA_SUCCESS,
      actionTypeFail: MENU_DATA_ERROR,
    });
}

export function getFilmsDataAction(url, test = false) {
  return (dispatch) =>
    getRequestApi({
      url,
      dispatch,
      actionType: GET_FILMDATA_SUCCESS,
      actionTypeFail: GET_FILMDATA_ERROR,
    });
}




