import {combineReducers} from 'redux';

import SideMenuReducers from './SideMenuReducers';

const appReducer = combineReducers({
  sideMenuReducers: SideMenuReducers,
});

export default rootReducer = (state, action) => {
  return appReducer(state, action);
};
