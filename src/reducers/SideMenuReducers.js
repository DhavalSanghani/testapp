import React from 'react';
import {
  MENU_DATA_SUCCESS,
  SUB_CATEGORY_DATA_SUCCESS,
  SUB_CATEGORY_DATA_ERROR,
  RESET,
  MENU_DATA_ERROR,
  GET_PRODUCT_DATA_SUCCESS,
  GET_PRODUCT_DATA_ERROR,
  GET_FILMDATA_SUCCESS,
  GET_FILMDATA_ERROR,
} from '../actions/Type';

const initialState = {
  responseCode: null,

  menuDataError: false,
  isTimeOut: false,
  menuData: null,
  status_code: null,
  subCategoryData: null,
  subCategoryError: false,

  product_status_code: null,
  productList: null,
  productList_error: false,
  filmsData: null,
  filmsDataError: false,
};

function SideMenuReducers(state = initialState, action) {
  switch (action.type) {
    case MENU_DATA_SUCCESS:
      response = action.payload;
      return {
        ...state,
        isLoading: false,
        responseCode: response.data.status_code,
        menuData: response.data.results,
      };

    case GET_FILMDATA_SUCCESS:
      response = action.payload;
      return {
        ...state,
        isLoading: false,
        // responseCode: response.data.status_code,
        filmsData: response.data,
      };
    case GET_FILMDATA_ERROR:
      response = action.payload;
      console.log('GET_FILMDATA_ERRROR--->', response.data);
      return {
        ...state,
        // responseCode: response.data.status_code,
        filmsDataError: true,
      };
    case MENU_DATA_ERROR:
      response = action.payload;
      return {
        ...state,
        menuDataError: true,
        // isTimeOut: response.request._timedOut,
      };

 

    case RESET: {
      switch (action.payload) {
        default:
          return {
            ...state,
            filmsData: null,
            responseCode: null,
            menuData: null,
            message: null,
            menuDataError: false,
            filmsDataError: false,
            status_code: null,
            subCategoryData: null,
            subCategoryError: false,
            product_status_code: null,
            productList: null,
            productList_error: false,
          };
      }
    }
  }
  return state;
}

export default SideMenuReducers;
