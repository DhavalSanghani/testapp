import {
  ACTION_FAIL,
} from '../../actions/Type';

import axios from 'axios';
import AsyncStorage from '@react-native-community/async-storage';

export const getRequestApi = ({
  url,
  dispatch,
  actionType,
  actionTypeFail = ACTION_FAIL,
}) => {
  // console.log('URL==>' + url);
  return axios
    .get(url)
    .then((response) => {
      console.log('====responseeeee22222====', response);
      dispatch({type: actionType, payload: response});
    })
    .catch((error) => {
      console.log('Error---->', error);
      dispatch({type: actionTypeFail, payload: error});
    });
};

