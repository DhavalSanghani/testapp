import {NavigationContainer, useNavigation} from '@react-navigation/native';
import React, {useState, useEffect} from 'react';

import {createStackNavigator} from '@react-navigation/stack';
import messaging from '@react-native-firebase/messaging';

import {createDrawerNavigator} from '@react-navigation/drawer';

import CustomDrawerContent from '../components/CustomDrawerContent';
import PeopleDetails from '../screens/PeopleDetails';
import FilmDetails from '../screens/FilmDetails';
import Home from '../screens/Home';
import Login from '../screens/Login';
import Register from '../screens/Register';

import Splash from '../screens/Splash';

const Drawer = createDrawerNavigator();
const Stack = createStackNavigator();

const Auth = () => {
  return (
    <Stack.Navigator headerMode="none" initialRouteName="Login">
      <Stack.Screen name="Login" component={Login} />
      <Stack.Screen name="Register" component={Register} />
    </Stack.Navigator>
  );
};

Route = () => {
  const [loading, setLoading] = useState(true);
  const [initialRoute, setInitialRoute] = useState('Splash');

  useEffect(() => {

    messaging().onNotificationOpenedApp((remoteMessage) => {
      console.log(
        'Notification caused app to open from background state:',
        remoteMessage.notification,
      );
    });

    messaging()
      .getInitialNotification()
      .then((remoteMessage) => {
        if (remoteMessage) {
          console.log(
            'Notification caused app to open from quit state:',
            remoteMessage.notification,
          );
          setInitialRoute(remoteMessage.data.type); 
        }
        setLoading(false);
      });
  }, []);

  if (loading) {
    return null;
  }

  createHomeStack = () => (
    <Stack.Navigator headerMode="none" initialRouteName={initialRoute}>
      <Stack.Screen name="Splash" component={Splash} />
      <Stack.Screen name="Auth" component={Auth} />
      <Stack.Screen name="Login" component={Login} />
      <Stack.Screen name="Home" component={createDrawer} />
      <Stack.Screen name="Register" component={Register} />

      <Stack.Screen name="PeopleDetails" component={PeopleDetails} />
      <Stack.Screen name="FilmDetails" component={FilmDetails} />
    </Stack.Navigator>
  );

  return <NavigationContainer>{this.createHomeStack()}</NavigationContainer>;
};

createDrawer = () => (
  <Drawer.Navigator
    drawerContent={(props) => <CustomDrawerContent {...props} />}>
    <Drawer.Screen
      // navigation={navigation}
      name="Home"
      component={Home}
      options={{drawerLabel: 'Home'}}
    />

    <Drawer.Screen
      name="PeopleDetails"
      component={PeopleDetails}
      options={{drawerLabel: 'PeopleDetails'}}
    />

    <Drawer.Screen
      // navigation={navigation}
      name="FilmDetails"
      component={FilmDetails}
      options={{drawerLabel: 'FilmDetails'}}
    />
  </Drawer.Navigator>
);

export default Route;
