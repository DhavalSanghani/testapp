/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {SafeAreaView} from 'react-native';

import {Provider} from 'react-redux';

import {createStore, applyMiddleware} from 'redux';
import ReduxThunk from 'redux-thunk';

import RootStack from './src/route/Route';
import reducerAll from './src/reducers';

const store = createStore(reducerAll, {}, applyMiddleware(ReduxThunk));

const App = () => {
  return (
    <Provider store={store}>
      <React.Fragment>
        <SafeAreaView style={{flex: 0, backgroundColor: 'black'}} />
        <SafeAreaView style={{flex: 1, backgroundColor: 'black'}}>
          {/* <SafeAreaView style={{flex: 1,backgroundColor:'transparent'}}> */}
          <RootStack />
        </SafeAreaView>
      </React.Fragment>
    </Provider>
  );
};

export default App;
